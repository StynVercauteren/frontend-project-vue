const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'departments', component: () => import('pages/Index.vue'), props: true },
      { path: 'departments/:id', name: 'Index', component: () => import('pages/Index.vue'), props: true },
      { path: 'patients/:id', name: 'patientDetail', component: () => import('pages/PatientDetail.vue'), props: true },
      { path: 'settings', name: 'Settings', component: () => import('pages/Settings.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
