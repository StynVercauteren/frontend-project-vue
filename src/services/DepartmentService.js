import axios from 'axios'

// const RESOURCE_PATH = 'http://localhost:3000/departments'
const RESOURCE_PATH = 'http://localhost:8081/departments'
const RESOURCE_PATH_ID = 'http://localhost:8081/departementen'

export default class DepartmentService {
  get () {
    return axios.get(RESOURCE_PATH).then(result => result.data)
  }
  /*
  getById (id) {
    return axios.get(RESOURCE_PATH + '/' + id).then(result => result.data)
  }
  */
  getById (id) {
    return axios.get(RESOURCE_PATH_ID + '/?id=' + id).then(result => result.data)
  }
}
