import axios from 'axios'

// const RESOURCE_PATH = 'http://localhost:3000/actions'
const RESOURCE_PATH = 'http://localhost:8081/actions'
const RESOURCE_PATH_ID = 'http://localhost:8081/acties'

export default class RoomService {
  add (action) {
    return axios.post(RESOURCE_PATH, { action: action }).then(result => result.data)
  }
  get () {
    return axios.get(RESOURCE_PATH).then(result => result.data)
  }
  getActionsByIds (ids) {
    return axios.get(RESOURCE_PATH_ID + '?' + ids).then(result => result.data)
  }
  /*
  delete (action) {
    return axios.delete(RESOURCE_PATH + '/' + action.id)
  }
  */
  delete (action) {
    return axios.post('http://localhost:8081/deleteAction', { actionId: action._id })
  }
  /*
  update (action) {
    return axios.put(RESOURCE_PATH + '/' + action._id, action)
  }
  */
  update (action) {
    return axios.put(RESOURCE_PATH, { action: action })
  }
}
