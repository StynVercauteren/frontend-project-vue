import axios from 'axios'

// const RESOURCE_PATH = 'http://localhost:3000/rooms'
const RESOURCE_PATH = 'http://localhost:8081/rooms'

export default class RoomService {
  get () {
    return axios.get(RESOURCE_PATH).then(result => result.data)
  }
  getRoomsByDepartment (departmentId) {
    return axios.get('http://localhost:8081/roomen' + '?department=' + departmentId).then(result => result.data)
  }
  getRoomByPatient (patientId) {
    return axios.get(RESOURCE_PATH + '?patientId=' + patientId).then(result => result.data)
  }
}
