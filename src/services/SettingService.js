import axios from 'axios'

// const RESOURCE_PATH = 'http://localhost:3000/settings'
const RESOURCE_PATH = 'http://localhost:8081/settings'

export default class SettingService {
  getSettingByName (name) {
    return axios.get(RESOURCE_PATH + '?name=' + name).then(result => result.data)
  }
  /*
  update (setting) {
    return axios.put(RESOURCE_PATH + '/' + setting.id, setting)
  }
  */
  update (setting) {
    return axios.put(RESOURCE_PATH, { setting: setting })
  }
}
