import axios from 'axios'

// const RESOURCE_PATH = 'http://localhost:3000/patients'
const RESOURCE_PATH = 'http://localhost:8081/patients'

export default class PatientService {
  getPatient (patientID) {
    return axios.get(RESOURCE_PATH + '/?id=' + patientID).then(result => result.data)
  }
  /*
  update (patient) {
    return axios.put(RESOURCE_PATH + '/' + patient.id, patient)
  }
  */
  update (patient) {
    return axios.put(RESOURCE_PATH, { patient: patient })
  }
}
